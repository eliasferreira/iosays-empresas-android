![N|Solid](logo_ioasys.png)

# README #

### Execultar Aplicação ###

Certifique-se de que tenha um smartphone conectado via USB ao computador ou  
um emulador android. O modo de desenvolvedor deve estar ativo.  

Dentro da pasta raiz da aplicação "Empresas" use os comandos.  


* Windows
	
	* gradlew tasks
	* gradlew assembleDebug
	* gradlew installDebug

* Mac ou Linux

	* ./gradlew tasks
	* ./gradlew assembleDebug
	* ./gradlew installDebug

* Ou atraves da IDE Android Studio https://developer.android.com/studio.
* Para mais informações acesse: https://developer.android.com/studio/build/building-cmdline

### Bibliotecas ###

* Retrofit: Biblioteca para manipulação de integração com Web Service.
* Retrofit-GSON: Biblioteca para corversão de dados em formato JSON para objetos vice e versa.
* Mockito: Para efetuar testes nas lógicas implementadas no sistema.
* Glide: Manipular imagens, insersão aos layouts, requisições e armazenamento em cash para não efetuar divrsas requisiçôes com as mesma urls.
* kotlinx-coroutines: Trabalhando em serviços em diferentes threads, exemplo à uma integração com um Web Service.

### Acabou o tempo! ###

Em caso de mais tempo disponivél, iria adicionar validaçôes mais complexas ao formulário do login,   
efetuar diversos testes unitários ultilizando o Mockito.  
Ajustar o layout de pesquisa pois não consegui deixalo como está no exemplo do zeplin.  
Solicitaria contato com o design para ideias sobre o layout.


	





