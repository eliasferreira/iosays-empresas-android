package br.com.iosays.empresas.ui.activity

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import br.com.iosays.empresas.R
import br.com.iosays.empresas.ui.controller.HomeActivityController
import br.com.iosays.empresas.ui.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel


class HomeActivity : AppCompatActivity() {
    private var homeActivityController: HomeActivityController? = null
    val loginViewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        Log.e("Headers", loginViewModel.getHeaders().value.toString())
        Log.e("User", loginViewModel.getUsuario().value.toString())
        enterprisesDataRequest()
    }


    private fun enterprisesDataRequest() {
        if (loginViewModel.getHeaders().value != null) {
            homeActivityController = HomeActivityController(
                homeActivity = this,
                headers = loginViewModel.getHeaders().value!!
            )
            homeActivityController?.run()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.search_menu, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu?.findItem(R.id.search_action)?.actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setIconifiedByDefault(false)
        }

        val searchAction = menu.findItem(R.id.search_action)


        val searchView = searchAction?.actionView as SearchView
        searchView.addOnAttachStateChangeListener(OnAttachStateChange(info_text_view))

        searchView.setOnQueryTextListener(homeActivityController)
        return true
    }

    class OnAttachStateChange(val view: View) : View.OnAttachStateChangeListener {

        override fun onViewDetachedFromWindow(v: View?) {
            GlobalScope.launch(context = Dispatchers.Main) {
                delay(125)
                view.visibility = View.VISIBLE
            }
        }

        override fun onViewAttachedToWindow(v: View?) {
            view.visibility = View.GONE
        }

    }
}
