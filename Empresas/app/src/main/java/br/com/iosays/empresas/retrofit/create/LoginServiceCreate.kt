package br.com.iosays.empresas.retrofit.create

import android.content.Context
import br.com.iosays.empresas.retrofit.RetrofitInitialize
import br.com.iosays.empresas.retrofit.service.LoginService

class LoginServiceCreate(context: Context) {

     val service = RetrofitInitialize(context = context)
        .retrofit
        .create(LoginService::class.java)
}