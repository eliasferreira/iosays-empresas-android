package br.com.iosays.empresas.ui.viewholder

import android.view.View
import br.com.iosays.empresas.R
import br.com.iosays.empresas.model.DataBind
import br.com.iosays.empresas.model.Enterprise
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.enterprise_layout.view.*

class EnterpriseViewHolder(itemView: View) : BaseViewHolder(itemView) {

    private lateinit var enterprise: Enterprise

    override fun bind(data: DataBind) {
        this.enterprise = data.data as Enterprise
        setDataOnView()
    }

    private fun setDataOnView() {

        itemView.apply {

//            enterprise_description.text = enterprise.description
//
//            val enterprisePhoto =
//                "${itemView.context.getString(R.string.base_api_url)}${enterprise.photo}"
//            Glide
//                .with(context)
//                .load(enterprisePhoto)
//                .placeholder(R.drawable.img_indisponivel)
//                .into(enterprise_image)
        }
    }
}

