package br.com.iosays.empresas.application

import android.app.Application
import br.com.iosays.empresas.data.ApiHeaderData
import br.com.iosays.empresas.data.ApiHeadersRepository
import br.com.iosays.empresas.retrofit.create.LoginServiceCreate
import br.com.iosays.empresas.ui.repository.LoginRepository
import br.com.iosays.empresas.ui.viewmodel.ApiHeadersViewModel
import br.com.iosays.empresas.ui.viewmodel.LoginViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module

class Application : Application() {

    private val apiHeadersModule = module {
        val apiHeadersRepository = ApiHeadersRepository.getInstance(ApiHeaderData())
        single { ApiHeadersViewModel(apiHeadersRepository) }
    }

    private val loginModule = module {
        val loginRepository = LoginRepository.getInstance()
        single { LoginViewModel(loginRepository) }
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@Application)
            modules(apiHeadersModule, loginModule)
        }
    }
}