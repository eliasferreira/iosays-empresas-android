package br.com.iosays.empresas.ui.controller

import android.util.Log
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.iosays.empresas.ui.adapter.EnterprisesAdapter
import br.com.iosays.empresas.model.Enterprises
import br.com.iosays.empresas.model.Headers
import br.com.iosays.empresas.retrofit.create.EnterpriseServiceCreate
import br.com.iosays.empresas.ui.activity.HomeActivity
import br.com.iosays.empresas.validation.internetState
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject

class HomeActivityController(
    private val homeActivity: HomeActivity,
    private val headers: Headers
) : SearchView.OnQueryTextListener {


    private var enterpriseAdapter: EnterprisesAdapter? = null

    fun run() {

        if (internetState(homeActivity)) {
            val service = EnterpriseServiceCreate(context = this.homeActivity).service

            CoroutineScope(Dispatchers.IO)
                    .launch {
                        val request = service.findAll(
                                accessToken = headers.accessToken,
                                client = headers.client,
                                uid = headers.uid
                        )
                        withContext(Dispatchers.Main) {
                            val response = request.await()

                            if (response.isSuccessful) {
                                response.body()?.let { recyclerViewSetData(it) }

                                println(response.body().toString())
                            } else {
                                val jsonObject = JSONObject(response.errorBody()?.string())

                                Log.e("Error", jsonObject.getString("errors"))
                            }
                        }
                    }
        }
    }

    private fun recyclerViewSetData(enterprises: Enterprises) {
        enterpriseAdapter = EnterprisesAdapter(activity = homeActivity, enterprises = enterprises.enterprises)
        homeActivity.recycler_view_enterprises.apply {
            layoutManager = LinearLayoutManager(homeActivity, RecyclerView.VERTICAL, false)
            adapter = enterpriseAdapter
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        enterpriseAdapter?.filter?.filter(newText)
        return false
    }
}