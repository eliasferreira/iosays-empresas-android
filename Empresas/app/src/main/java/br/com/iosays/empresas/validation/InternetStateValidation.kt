package br.com.iosays.empresas.validation

import android.content.Context
import android.net.ConnectivityManager

/**
 **@Method to check if the device have an internet connection
 **/

fun internetState(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?

    if (connectivityManager != null) {
        val activeNetwork = connectivityManager.getActiveNetworkInfo()
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI
                    || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return true
            }
        }
    }

    return false
}