package br.com.iosays.empresas.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.iosays.empresas.R
import br.com.iosays.empresas.model.DataBind
import br.com.iosays.empresas.ui.viewholder.BaseViewHolder
import br.com.iosays.empresas.ui.viewholder.EnterpriseViewHolder

class EnterpriseAdapter(dataBind: DataBind) : AppAdapter(listOf(dataBind)) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {

        return EnterpriseViewHolder(
            itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.enterprise_layout, parent, false)
        )
    }
}