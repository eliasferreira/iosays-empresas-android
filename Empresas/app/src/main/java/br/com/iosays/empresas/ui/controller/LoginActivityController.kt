package br.com.iosays.empresas.ui.controller

import android.app.Activity
import android.content.Intent
import android.view.View
import android.view.inputmethod.InputMethodManager
import br.com.iosays.empresas.R
import br.com.iosays.empresas.model.Authenticater
import br.com.iosays.empresas.model.Authenticates
import br.com.iosays.empresas.ui.activity.HomeActivity
import br.com.iosays.empresas.ui.activity.LoginActivity
import br.com.iosays.empresas.ui.validation.LoginValidation
import br.com.iosays.empresas.validation.internetState
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivityController(val activity: LoginActivity) : Authenticater {


    init {
        activity.form_login_butto.setOnClickListener {
            hideSoftKeyboard()
            if (LoginValidation(activity).isValid()) {
                run()
            }
        }
    }

    override fun onAuthenticatesResult(isSuccessFull: Boolean, httpCode: Int) {
        when {
            isSuccessFull -> startHomeActivity()
            httpCode == 401 -> showMessage(message = activity.getString(R.string.unauthorized))
            else -> showMessage(message = activity.getString(R.string.error))
        }
    }

    fun run() {
        if (internetState(activity)) {
            showProgress()
            val authenticates = Authenticates(
                email = activity.user_login_email.text.toString().trim(),
                password = activity.user_login_password.text.toString().trim()
            )
            activity.loginViewModel.init(context = activity, authenticates = authenticates, authenticater = this)
        } else {
            showMessage(message = activity.getString(R.string.out_of_connection))
        }
    }

    private fun showProgress() {
        activity.user_login_email.isEnabled = false
        activity.user_login_password.isEnabled = false
        activity.form_login_butto.isEnabled = false
        activity.login_form_text.visibility = View.GONE
        activity.logign_progress_bar.visibility = View.VISIBLE
    }

    private fun resetView() {
        activity.user_login_email.isEnabled = true
        activity.user_login_password.isEnabled = true
        activity.form_login_butto.isEnabled = true
        activity.login_form_text.visibility = View.VISIBLE
        activity.logign_progress_bar.visibility = View.GONE
    }

    private fun showMessage(message: String) {
        Snackbar.make(activity.login_view_container, message, Snackbar.LENGTH_LONG).show()
    }

    private fun startHomeActivity() {
        val intent = Intent(this@LoginActivityController.activity, HomeActivity::class.java)
        this@LoginActivityController.activity.apply {
            startActivity(intent)
            resetView()
            finish()
        }
    }

    fun hideSoftKeyboard() {
        val inputMethodManager = activity.getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            activity.currentFocus!!.windowToken, 0
        )
    }
}