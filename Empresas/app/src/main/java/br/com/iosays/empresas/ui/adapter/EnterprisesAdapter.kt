package br.com.iosays.empresas.ui.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.recyclerview.widget.RecyclerView
import br.com.iosays.empresas.R
import br.com.iosays.empresas.model.Enterprise
import br.com.iosays.empresas.ui.ENTERPRISE_KEY
import br.com.iosays.empresas.ui.activity.EnterpriseActivity
import br.com.iosays.empresas.ui.activity.HomeActivity
import br.com.iosays.empresas.ui.activity.VIEW_NAME_HEADER_IMAGE
import br.com.iosays.empresas.ui.activity.VIEW_NAME_HEADER_TITLE
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.enterprise_list_layout.view.*

class EnterprisesAdapter(val activity: Activity, val enterprises: List<Enterprise>) :
    RecyclerView.Adapter<EnterprisesAdapter.ViewHolder>(), Filterable {

    val enterprisesFilteredList = ArrayList<Enterprise>(enterprises)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            activity = activity,
            itemView = LayoutInflater.from(parent.context).inflate(
                R.layout.enterprise_list_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return enterprisesFilteredList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(
            enterprise = enterprisesFilteredList[position],
            position = position,
            itensSize = itemCount
        )
    }

    class ViewHolder(val activity: Activity, itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(enterprise: Enterprise, position: Int, itensSize: Int) {

            itemView.setOnClickListener {
                val intent = Intent(itemView.context, EnterpriseActivity::class.java)
                intent.putExtra(ENTERPRISE_KEY, enterprise)
                val activityOptionsCompat =
                    ActivityOptionsCompat.makeSceneTransitionAnimation(
                        activity,
                        Pair<View, String>(itemView.enterprise_image, VIEW_NAME_HEADER_IMAGE),
                        Pair<View, String>(itemView.enterprise_name, VIEW_NAME_HEADER_TITLE)
                    )
                ActivityCompat.startActivity(itemView.context, intent, activityOptionsCompat.toBundle())
            }

            itemView.enterprise_name.text = enterprise.enterpriseName
            itemView.enterprise_country.text = enterprise.country
            itemView.enterprise_type.text = enterprise.enterpriseType.enterpriseTypeName

            val enterprisePhoto =
                "${itemView.context.getString(R.string.base_api_url)}${enterprise.photo}"

            Glide.with(itemView.context)
                .load(enterprisePhoto)
                .placeholder(R.drawable.img_indisponivel)
                .into(itemView.enterprise_image)

            setItemViewMargin(position = position, itensSize = itensSize)


        }

        private fun setItemViewMargin(position: Int, itensSize: Int) {
            var params = itemView.layoutParams as ViewGroup.MarginLayoutParams
            var density = itemView.context.resources.displayMetrics.density
            var bottonMargin: Int
            var topMargin: Int
            if (position == 0) {
                topMargin = (8 * density).toInt()
            } else {
                topMargin = (4 * density).toInt()
            }
            if (position == itensSize - 1) {
                bottonMargin = (16 * density).toInt()
            } else {
                bottonMargin = 0

            }
            params.setMargins(params.leftMargin, topMargin, params.rightMargin, bottonMargin)
            itemView.layoutParams = params

        }

    }

    override fun getFilter(): Filter {

        return EnterpriseFilter(this)
    }

    private class EnterpriseFilter(val enterprisesAdapter: EnterprisesAdapter) : Filter() {

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filteredEnterprises = arrayListOf<Enterprise>()

            if (constraint.isNullOrEmpty()) {
                filteredEnterprises.addAll(enterprisesAdapter.enterprises)
            } else {
                val filterValue = constraint.toString().toLowerCase().trim()

                enterprisesAdapter.enterprises.forEach { enterprise ->
                    if (enterprise.enterpriseName.toLowerCase().contains(filterValue)) {
                        filteredEnterprises.add(enterprise)
                    }
                }
            }

            val filterResults = FilterResults()
            filterResults.values = filteredEnterprises

            return filterResults

        }

        @Suppress("UNCHECKED_CAST")
        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            enterprisesAdapter.enterprisesFilteredList.clear()
            val resultEnterprises = results?.values as ArrayList<Enterprise>

            enterprisesAdapter.enterprisesFilteredList.addAll(resultEnterprises)
            enterprisesAdapter.notifyDataSetChanged()
        }

    }


}