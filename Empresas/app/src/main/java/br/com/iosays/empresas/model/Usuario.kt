package br.com.iosays.empresas.model

class Usuario (
    val enterprise: Enterprise,
    val success: Boolean,
    val investor: Investor
) {
    override fun toString(): String {
        return "Usuario(enterprise=$enterprise, success=$success, investor=$investor)"
    }
}