package br.com.iosays.empresas.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.iosays.empresas.R
import br.com.iosays.empresas.ui.controller.LoginActivityController
import br.com.iosays.empresas.ui.viewmodel.LoginViewModel
import org.koin.android.viewmodel.ext.android.viewModel


class LoginActivity : AppCompatActivity() {

    val loginViewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        LoginActivityController(activity = this)
    }
}
