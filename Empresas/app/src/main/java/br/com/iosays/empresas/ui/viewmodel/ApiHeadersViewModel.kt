package br.com.iosays.empresas.ui.viewmodel

import androidx.lifecycle.ViewModel
import br.com.iosays.empresas.data.ApiHeadersRepository
import br.com.iosays.empresas.model.Headers

class ApiHeadersViewModel(private val apiHeadersRepository: ApiHeadersRepository) : ViewModel() {

    fun getApiHeaders() = apiHeadersRepository.getApiHeaders()
    fun setApiHeaders(headers: Headers) = apiHeadersRepository.setApiHeaders(headers)

}