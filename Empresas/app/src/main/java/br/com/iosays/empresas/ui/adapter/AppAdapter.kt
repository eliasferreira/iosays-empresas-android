package br.com.iosays.empresas.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import br.com.iosays.empresas.model.DataBind
import br.com.iosays.empresas.ui.viewholder.BaseViewHolder


abstract class AppAdapter(var itens: List<DataBind>) : RecyclerView.Adapter<BaseViewHolder>() {

    override fun getItemViewType(position: Int): Int {
        return itens[position].viewHolderID
    }

    override fun getItemCount(): Int {
        return itens.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(itens[position])
    }
}