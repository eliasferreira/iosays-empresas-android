package br.com.iosays.empresas.data

import br.com.iosays.empresas.model.Headers


class ApiHeadersRepository private constructor(private val apiHeaderData: ApiHeaderData) {

    fun getApiHeaders() = apiHeaderData.headers
    fun setApiHeaders(headers: Headers) {
        apiHeaderData.headers = headers
    }

    companion object {
        @Volatile
        private var instance: ApiHeadersRepository? = null

        fun getInstance(apiHeaderData: ApiHeaderData) =
            instance ?: synchronized(this) {
                instance ?: ApiHeadersRepository(apiHeaderData).also { instance = it }
            }
    }
}