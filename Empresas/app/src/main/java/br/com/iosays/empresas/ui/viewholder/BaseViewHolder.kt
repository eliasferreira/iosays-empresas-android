package br.com.iosays.empresas.ui.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.iosays.empresas.model.DataBind

abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(data: DataBind)
}