package br.com.iosays.empresas.retrofit

import android.content.Context
import br.com.iosays.empresas.R
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitialize(context: Context) {

    val retrofit = Retrofit.Builder()
        .baseUrl(context.getString(R.string.base_api_url))
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

}