package br.com.iosays.empresas.model

import com.google.gson.annotations.SerializedName

class Investor(
    val id: Int?,

    @SerializedName("investor_name")
    val investorName: String,
    val email: String,
    val city: String,
    val country: String,
    val balance: Long,
    val photo: String,

    @SerializedName("portfolio_value")
    val portfolioValue: Long,

    @SerializedName("first_access")
    val firstAccess: Boolean,

    @SerializedName("super_angel")
    val superAngel: Boolean,
    val portfolio: Portfolio
)