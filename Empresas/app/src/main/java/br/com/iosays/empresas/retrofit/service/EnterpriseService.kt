package br.com.iosays.empresas.retrofit.service

import br.com.iosays.empresas.model.Enterprise
import br.com.iosays.empresas.model.Enterprises
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface EnterpriseService {


    @GET(value = "api/v1/enterprises/")
    fun findAll(
        @Header("access-token") accessToken: String?,
        @Header("client") client: String?,
        @Header("uid") uid: String?
    ): Deferred<Response<Enterprises>>


    @GET(value = "api/v1/enterprises/{id}")
    fun find(
        @Header("access-token") accessToken: String?,
        @Header("client") client: String?,
        @Header("uid") uid: String?,
        @Path("id") id: Int
    ): Deferred<Response<Enterprise>>
}