package br.com.iosays.empresas.ui.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.iosays.empresas.model.Authenticater
import br.com.iosays.empresas.model.Authenticates
import br.com.iosays.empresas.model.Headers
import br.com.iosays.empresas.model.Usuario
import br.com.iosays.empresas.retrofit.create.LoginServiceCreate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class LoginRepository private constructor() {

    private lateinit var usuario: LiveData<Usuario>
    private lateinit var headers: LiveData<Headers>

    companion object {
        @Volatile
        private var instance: LoginRepository? = null

        fun getInstance() =
            instance ?: LoginRepository().also { instance = it }
    }

    fun getHeaders(): LiveData<Headers> {
        return this.headers
    }

    fun getUsuario(): LiveData<Usuario> {
        return this.usuario
    }

    fun authenticates(
        authenticater: Authenticater,
        authenticates: Authenticates,
        context: Context
    ) {
        val loginService = LoginServiceCreate(context = context).service
        CoroutineScope(Dispatchers.IO)
            .launch {
                val request = loginService.login(authenticates = authenticates)
                withContext(Dispatchers.Main) {
                    val response = request.await()

                    if (response.isSuccessful) {
                        val headers = MutableLiveData<Headers>()
                        val buildHeaders = buildHeaders(response)
                        headers.value = buildHeaders

                        val usuario = MutableLiveData<Usuario>()
                        usuario.value = response.body()
                        this@LoginRepository.usuario = usuario
                        this@LoginRepository.headers = headers
                    }
                    authenticater.onAuthenticatesResult(response.isSuccessful, response.code())
                }
            }
    }

    private fun buildHeaders(response: Response<Usuario>): Headers {
        val headers = Headers(
            accessToken = response.headers().get("access-token"),
            client = response.headers().get("client"),
            uid = response.headers().get("uid")
        )
        return headers
    }
}