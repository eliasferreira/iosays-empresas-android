package br.com.iosays.empresas.model

class DataBind(
    val data: Any,
    val viewHolderID: Int
)