package br.com.iosays.empresas.ui.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import br.com.iosays.empresas.model.Authenticates
import br.com.iosays.empresas.model.Headers
import br.com.iosays.empresas.model.Usuario
import br.com.iosays.empresas.ui.controller.LoginActivityController
import br.com.iosays.empresas.ui.repository.LoginRepository

class LoginViewModel(val loginRepository: LoginRepository) : ViewModel() {

    fun init(
        context: Context,
        authenticates: Authenticates,
        authenticater: LoginActivityController
    ) {
        loginRepository.authenticates(context = context, authenticates = authenticates, authenticater = authenticater)
    }

    fun getHeaders(): LiveData<Headers> {
        return loginRepository.getHeaders()
    }

    fun getUsuario(): LiveData<Usuario> {
        return loginRepository.getUsuario()
    }
}