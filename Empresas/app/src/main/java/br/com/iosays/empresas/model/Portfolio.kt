package br.com.iosays.empresas.model

import com.google.gson.annotations.SerializedName

class Portfolio (

    @SerializedName("enterprises_number")
    val enterPrisesNumber: Int,
    val enterprises: List<Enterprise>
)