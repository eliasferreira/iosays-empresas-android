package br.com.iosays.empresas.ui.activity

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import br.com.iosays.empresas.R
import br.com.iosays.empresas.model.Enterprise
import br.com.iosays.empresas.ui.ENTERPRISE_KEY
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.container_layout.*


// View name of the header image. Used for activity scene transitions
val VIEW_NAME_HEADER_IMAGE = "detail:header:image"

// View name of the header title. Used for activity scene transitions
val VIEW_NAME_HEADER_TITLE = "detail:header:title"

class EnterpriseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.container_layout)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        intentCheckKey()
        ViewCompat.setTransitionName(enterprise_image, VIEW_NAME_HEADER_IMAGE)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    private fun intentCheckKey() {

        if (intent.hasExtra(ENTERPRISE_KEY)) {
            val enterprise = intent.getSerializableExtra(ENTERPRISE_KEY) as Enterprise

            supportActionBar?.title = enterprise.enterpriseName

//            recycler_view_container_layout.apply {
//                layoutManager =
//                    LinearLayoutManager(this@EnterpriseActivity, RecyclerView.VERTICAL, false)
//                adapter = EnterpriseAdapter(
//                    DataBind(
//                        viewHolderID = 0,
//                        data = enterprise
//                    )
//                )
//            }

            enterprise_description.text = enterprise.description

            val enterprisePhoto =
                "${getString(R.string.base_api_url)}${enterprise.photo}"
            Glide
                .with(this)
                .load(enterprisePhoto)
                .placeholder(R.drawable.img_indisponivel)
                .into(enterprise_image)

        }
    }
}