package br.com.iosays.empresas.model

class ErrorResponse(
        val success: Boolean,
        val errors: List<String>
)