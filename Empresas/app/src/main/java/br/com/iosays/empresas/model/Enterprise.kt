package br.com.iosays.empresas.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Enterprise(
    val id: Int,

    @SerializedName("email_enterprise")
    val emailEnterPrise: String,
    val facebook: String,
    val twiter: String,
    val linkedin: String,
    val phone: String,

    @SerializedName("own_enterprise")
    val ownEnterprise: Boolean,

    @SerializedName("enterprise_name")
    val enterpriseName: String,
    val photo: String,
    val description: String,
    val city: String,
    val country: String,
    val value: Int,

    @SerializedName("share_price")
    val sharePrice: Double,

    @SerializedName("enterprise_type")
    val enterpriseType: EnterpriseType
): Serializable
