package br.com.iosays.empresas.retrofit.service

import br.com.iosays.empresas.model.Authenticates
import br.com.iosays.empresas.model.Usuario
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService {

    @POST(value = "api/v1/users/auth/sign_in")
    fun login(@Body authenticates: Authenticates): Deferred<Response<Usuario>>
}