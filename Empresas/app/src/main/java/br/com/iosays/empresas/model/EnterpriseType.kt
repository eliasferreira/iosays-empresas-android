package br.com.iosays.empresas.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class EnterpriseType(
    val id: Int,

    @SerializedName("enterprise_type_name")
    val enterpriseTypeName: String
): Serializable
