package br.com.iosays.empresas.ui.validation

import android.text.Editable
import android.text.TextWatcher
import android.widget.AutoCompleteTextView
import br.com.iosays.empresas.R
import br.com.iosays.empresas.ui.activity.LoginActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginValidation(val loginActivity: LoginActivity) {

    fun isValid(): Boolean {
        var hasNoErros = true

        val userEmail = loginActivity.user_login_email
        val userPassword = loginActivity.user_login_password

        userEmail.addTextChangedListener(OnTextChanged(userEmail))
        userPassword.addTextChangedListener(OnTextChanged(userPassword))

        val emailText = userEmail.text.trim()
        val passordText = userPassword.text.trim()


        if (emailText.isEmpty()) {
            userEmail.error = loginActivity.getString(R.string.required_field)
            hasNoErros = false
        } else if (!emailText.contains("@") || emailText.length < 7) {
            userEmail.error = loginActivity.getString(R.string.unavalable_email)
            hasNoErros = false
        } else {
            userEmail.error = null
        }

        if (passordText.isEmpty()) {
            userPassword.error = loginActivity.getString(R.string.password_required)
            hasNoErros = false
        } else {
            userPassword.error = null
        }

        return hasNoErros
    }


    private class OnTextChanged(val autoCompleteTextView: AutoCompleteTextView) : TextWatcher {

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            autoCompleteTextView.error = null
        }

        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }
    }
}