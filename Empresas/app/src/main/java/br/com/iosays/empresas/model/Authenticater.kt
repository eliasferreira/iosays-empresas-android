package br.com.iosays.empresas.model

interface Authenticater {
    fun onAuthenticatesResult(isSuccessFull: Boolean, httpCode: Int)
}