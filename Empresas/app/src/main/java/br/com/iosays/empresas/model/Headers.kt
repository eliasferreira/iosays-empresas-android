package br.com.iosays.empresas.model

class Headers(
    val accessToken: String?,
    val client: String?,
    val uid: String?
) {

    override fun toString(): String {
        return "Headers(accessToken=$accessToken, client=$client, uid=$uid)"
    }
}